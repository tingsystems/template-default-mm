(function () {
    'use strict';

    function Routes($stateProvider, $urlRouterProvider, $locationProvider) {

        $stateProvider
            .state('index', {
                url: '/',
                abstract: true,
                templateUrl: '/static/app/index.html',
                controllerAs: 'Home',
                controller: 'HomeCtrl'
            })
            .state('home', {
                url: '/',
                views: {
                    'content': {
                        templateUrl: '/templates/catalog-list.html',
                        controllerAs: 'Item',
                        controller: 'ItemCtrl'
                    }
                }
            })
            .state('item_detail', {
                url: '/:slug\.html',
                views: {
                    'content': {
                        templateUrl: '/templates/item-detail.html',
                        controllerAs: 'Item',
                        controller: 'ItemDetailCtrl'
                    }
                }
            })
            .state('cart', {
                url: '/cart',
                views: {
                    'content': {
                        templateUrl: '/templates/cart.html',
                        controllerAs: 'Cart',
                        controller: 'CartCtrl'
                    }
                }
            })
            .state('success_paid', {
                url: '/success',
                views: {
                    'content': {
                        templateUrl: '/templates/success-paid.html'
                    }
                }
            });

        $urlRouterProvider.otherwise('/');
        $locationProvider.html5Mode(true);
    }

    function AppConfig($httpProvider, blockUIConfig) {
        // Change the default overlay message
        blockUIConfig.message = 'Cargando...';

        // Change the default delay to 100ms before the blocking is visible
        blockUIConfig.delay = 90;

        // Set interceptor
        $httpProvider.interceptors.push('HttpInterceptor');

    }

    /**
     * @name Run
     * @desc Update xsrf $http headers to align with Django's defaults
     */
    function Run($http, $rootScope, $state, $window, $location, $anchorScroll) {
        $rootScope.$state = $state;
        //$rootScope.host = 'http://localhost:4000';
        $rootScope.host = 'https://mercadomovil.com.mx';
        $rootScope.shopId = 'changeThisSiteId';
        $rootScope.shopName = 'changeThisShopName';
        $rootScope.shopDescription = '';
        $rootScope.shopPhone = 'changeThisPhone';
        $rootScope.shopAddress = 'changeThisAddress';
        $rootScope.apiV = 'v2';

        // initialise google analytics
        //$window.ga('create', 'UA-53555832-28', 'default');
        // do something when change state

        // init for page title
        $rootScope.pageTitle = '';
        $rootScope.ShopMode = false;

    }

    angular.module('annalise', ['ui.router', 'ts.controllers', 'ts.directives', 'ngSanitize', 'app.templates',
        'infinite-scroll', 'akoenig.deckgrid', 'ngAnimate', 'ui.bootstrap', 'blockUI', 'ngStorage', 'angular-toasty',
        'timer', 'ui.select', 'ngMessages'])
        .config(Routes)
        .config(AppConfig)
        .run(Run);

    Run.$inject = ['$http', '$rootScope', '$state', '$window', '$location', '$anchorScroll'];
    Routes.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];
    AppConfig.$inject = ['$httpProvider', 'blockUIConfig'];
})();
