(function () {
    'use strict';

    // service for build the base url taking the host and siteId
    function BaseUrl() {
        return {
            get: function () {
                return '#host#/api/{{apiV}}/shop/{{shopId}}/';
            }
        }
    }

    function ChargeSrv($resource, BaseUrl) {
        return $resource(BaseUrl.get() + 'charges/:id', null, {})
    }

    function ItemSrv($resource, BaseUrl) {
        return $resource(BaseUrl.get() + 'items/:slug', null, {})
    }

    // service for show notifications with toasty
    function NotificationSrv(toasty) {
        return {
            success: function (msg, title) {
                toasty.success({
                    title: title,
                    msg: msg,
                    showClose: true,
                    clickToClose: true,
                    timeout: 6000,
                    sound: false,
                    theme: 'material'
                });
            },
            error: function (msg, title) {
                toasty.error({
                    title: !title ? 'Error' : title,
                    msg: msg,
                    showClose: true,
                    clickToClose: true,
                    timeout: 8000,
                    sound: false,
                    theme: 'material'
                });
            }
        }
    }

    // get all the states
    function StateSrv($resource) {
        var BaseUrl = 'http://geo.tingsystems.com/api/v1/';
        return $resource(BaseUrl + 'states', null, {
            // get cities, if state param get city by state id, returns an Array
            'getCities': { method: 'GET', url: BaseUrl + 'cities', isArray: true },
            // get cities, if state param get city by state id, returns an object
            'getCitiesObj': { method: 'GET', url: BaseUrl + 'cities' }
        });
    }


    // Add interceptor
    function HttpInterceptor($q, $rootScope) {
        return {
            // On request success
            request: function (config) {
                // console.log(config); // Contains the data about the request before it is sent.
                if (config.url.match('#host#/api/{{apiV}}/auth/login/tw') || '#host#/api/{{apiV}}/auth/login/fb') {
                    $rootScope.socialAuthData = config.data;
                }
                config.url = config.url.replace('{{shopId}}', $rootScope.shopId);
                config.url = config.url.replace('{{apiV}}', $rootScope.apiV);
                config.url = config.url.replace('#host#', $rootScope.host);
                // Return the config or wrap it in a promise if blank.
                return config || $q.when(config);
            },

            // On request failure
            requestError: function (rejection) {
                // console.log(rejection); // Contains the data about the error on the request.
                // Return the promise rejection.
                return $q.reject(rejection);
            },

            // On response success
            response: function (response) {
                // console.log(response); // Contains the data from the response.
                // Return the response or promise.
                return response || $q.when(response);
            },

            // On response failture
            responseError: function (rejection) {
                // console.log(rejection); // Contains the data about the error.

                // net::ERR_CONNECTION_REFUSED
                if (rejection.status == -1) {
                    NotificationSrv.error('Por favor intenta de nuevo hay problemas para establecer la conexión.');
                }
                // FORBIDDEN, UNAUTHORIZED
                if (rejection.status == 403 || rejection.status == 401) {
                    // logout the current user and delete the local storage
                    $rootScope.$emit('UNAUTHORIZED');
                }

                // 404, 500 error
                if (rejection.status == 404) {
                    $rootScope.$emit('HTTP_ERROR', { error: '404' });
                }

                if (rejection.data) {
                    // show form errors
                    if (rejection.data.form_errors) {
                        // display error messages
                        var messages, form_errors = rejection.data.form_errors;
                        for (var field in form_errors) {
                            messages = form_errors[field];
                            for (var i = 0, len = messages.length; i < len; i++) {
                                NotificationSrv.error(messages[i], field);
                            }
                        }
                    }
                    // show error messages
                    if (rejection.data.messages) {
                        var messages = rejection.data.messages;
                        for (var i = 0; i < messages.length; i++) {
                            NotificationSrv.error(messages[i]);
                        }
                    }
                }

                // Return the promise rejection.
                return $q.reject(rejection);
            }
        };
    }

    // Assign factory to module
    angular.module('ts.services', ['ngResource'])
        .factory('ItemSrv', ItemSrv)
        .factory('ChargeSrv', ChargeSrv)
        .factory('BaseUrl', BaseUrl)
        .factory('NotificationSrv', NotificationSrv)
        .factory('StateSrv', StateSrv)
        .factory('HttpInterceptor', HttpInterceptor);

    // Inject factory the dependencies
    ItemSrv.$inject = ['$resource', 'BaseUrl'];
    ChargeSrv.$inject = ['$resource', 'BaseUrl'];
    NotificationSrv.$inject = ['toasty'];
    StateSrv.$inject = ['$resource'];
    HttpInterceptor.$inject = ['$q', '$rootScope'];
})();